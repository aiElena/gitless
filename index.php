<?php
/**
 * sum number
 * @param float $a
 * @param float $b
 * @return string
 * @autor Elena Aleksandrova <ai.alexandrova@ukr.net>
 */
function sum(float $a, float $b) : float
{
    return $a + $b;
}

sum(3, 5);